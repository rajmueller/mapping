# mapping

Download and display map tiles using Matplotlib.
Provides some functionality of Matplotlib where x and y coordinates as arguments are replaced by lat and lon.

Bitmaps will be saved in `~/Data/Tiles` for future offline use.

Base on this example, although the underlying x and y coordinates do not depend on the zoom level:

* https://stackoverflow.com/questions/28476117/easy-openstreetmap-tile-displaying-for-python#28530369
* https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames

## Sources for map tiles

These sites list some sources of mapping tiles:

* https://openmaptiles.com/
* https://wiki.openstreetmap.org/wiki/Tiles

Some example url templates (where {0}, {1} and {2} are zoom level, x and y respectively):

```
"http://tile.openstreetmap.org/{0}/{1}/{2}.png",
"http://a.tiles.wmflabs.org/hillshading/{0}/{1}/{2}.png",
"http://a.tile.stamen.com/toner-lines/{0}/{1}/{2}.png",
"http://b.tile.stamen.com/toner-hybrid/{0}/{1}/{2}.png",
"http://b.tile.stamen.com/toner-labels/{0}/{1}/{2}.png",
"http://c.tile.stamen.com/terrain/{0}/{1}/{2}.png",
"http://c.tile.stamen.com/toner/{0}/{1}/{2}.png",
"http://c.tile.stamen.com/watercolor/{0}/{1}/{2}.jpg",
"https://korona.geog.uni-heidelberg.de/tiles/roads/x={1}&y={2}&z={0}",
"https://maps-for-free.com/layer/relief/z{0}/row{2}/{0}_{1}-{2}.jpg",
"https://services.arcgisonline.com/ArcGIS/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{0}/{2}/{1}.jpg",
"https://services.geodataonline.no/arcgis/rest/services/Geocache_WMAS_WGS84/GeocacheBasis/MapServer/tile/{0}/{2}/{1}",
"https://tiles.openseamap.org/seamark/{0}/{1}/{2}.png",
```