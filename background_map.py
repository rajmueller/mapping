import numpy as np
import os
from urllib import request
from io import BytesIO
from PIL import Image

import matplotlib as mpl
# mpl.use('TkAgg')  # anti-grain geometry
import matplotlib.pyplot as plt

# console (as opposed to jupyter)
# used to output stuff that shouldn't clutter jupyther
if os.name == 'nt':
    con = open('con', 'w')


def lon2num(lon_deg, zoom=None):
    """x_tile"""
    return (lon_deg + 180.0) / 360.0


def lat2num(lat_deg, zoom=None):
    """y_tile"""
    lat_rad = lat_deg / 180 * np.pi
    return (1.0 - np.log(np.tan(lat_rad) + (1 / np.cos(lat_rad))) / np.pi
            ) / 2.0


def deg2num(lat_deg, lon_deg, zoom):
    return (
        np.array(lon2num(lon_deg) * 2 ** zoom // 1).astype(int),
        np.array(lat2num(lat_deg) * 2 ** zoom // 1).astype(int),
    )


def num2deg(xtile, ytile, zoom):
    """
    http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    This returns the NW-corner of the square.
    Use the function with xtile+1 and/or ytile+1 to get the other corners.
    With xtile+0.5 & ytile+0.5 it will return the center of the tile.
    """
    lon_deg = xtile * 360.0 - 180.0
    lat_rad = np.arctan(np.sinh(np.pi * (1 - 2 * ytile)))
    lat_deg = np.degrees(lat_rad)
    return (lat_deg, lon_deg)


def decdeg2dms(dd, sign=False, precision=0):
    dd = round((((dd + 180) % 360) - 180) * 3600, precision)
    if sign:
        sign = np.sign(dd)
        mnt, sec = divmod(sign * dd, 60)
        deg, mnt = divmod(mnt, 60)
        return int(sign), int(deg), int(mnt), sec
    else:
        mnt, sec = divmod(dd, 60)
        deg, mnt = divmod(mnt, 60)
        return int(deg), int(mnt), sec


def getImageCluster(
    lat_min, lat_max, lon_min, lon_max, zoom, out_file=None,
    smurl=r"http://tile.openstreetmap.org/{0}/{1}/{2}.png"
):
    assert(zoom >= 0)
    tile_dir = os.path.join(
        os.environ['homedrive'] + os.environ['homepath'],
        'Data', 'Tiles', ''
    )
    if not os.path.isdir(tile_dir):
        os.mkdir(tile_dir)
    tile_dir += '_'.join(smurl.split(':')[1].split('/')[:-3])
    if not os.path.isdir(tile_dir):
        os.mkdir(tile_dir)
    xmin, ymax = deg2num(lat_min, lon_min, zoom)
    xmax, ymin = deg2num(lat_max, lon_max, zoom)

    bbox_ul = np.array([xmin, ymin]) / 2 ** zoom
    bbox_lr = np.array([xmax + 1, ymax + 1]) / 2 ** zoom

    Cluster = Image.new('RGBA', ((xmax - xmin + 1) * 256 -
                                 1, (ymax - ymin + 1) * 256 - 1))
    for xtile in range(xmin, xmax + 1):
        xtile_mod = xtile % (2 ** zoom)
        for ytile in range(ymin, ymax + 1):
            tile_name = f"{tile_dir}/{zoom}_{xtile_mod}_{ytile}.png"
            try:
                if os.path.isfile(tile_name):
                    tile = Image.open(tile_name)
                else:
                    imgurl = smurl.format(zoom, xtile_mod, ytile)
                    print("Opening: " + imgurl, file=out_file)
                    imgstr = request.urlopen(imgurl).read()
                    tile = Image.open(BytesIO(imgstr))
                    tile.save(tile_name)
                # if np.any(tile.reshape(-1)):
                Cluster.paste(tile, box=(
                    (xtile - xmin) * 255, (ytile - ymin) * 255))
            except Exception as E:
                print(f"Couldn't download image; {E}", file=out_file)
                tile = None

    return Cluster, [bbox_ul[0], bbox_lr[0], bbox_lr[1], bbox_ul[1]]


def get_dms_ticks(lim, step, dg_fmt='%+d\u00B0', mn_fmt=" %02d'",
                  sc_fmt=" %02.0f''", precision=0):
    ticks = step * (
        lim[0] // step + np.arange(0, (lim[1] - lim[0]) // step + 2)
    )
    ticklabels = []
    for t in ticks:
        pm, dg, mn, sc = decdeg2dms(t, sign=True)
        dg *= pm
        tl = ''
        for fmt, val in (dg_fmt, dg), (mn_fmt, mn), (sc_fmt, sc):
            if fmt:
                tl += fmt % val
        ticklabels.append(tl)
    return (ticks, ticklabels)


class map:
    def __init__(
        self, ax=None, lat_lim=(10, 100), lon_lim=(10, 50),
        lat_step=10, lon_step=10, zoom=None,
        smurl=(r"http://tile.openstreetmap.org/{0}/{1}/{2}.png", ),
        out_file=None, do_label=True, dist=None,
        dg_fmt='%+d\u00B0', xmn_fmt=" %02d'", ymn_fmt="\n%02d'",
        sc_fmt="", precision=0,
    ):
        self.ax = ax
        self._lat_lim = lat_lim = np.array(lat_lim)
        self._lon_lim = lon_lim = np.array(lon_lim)
        self._lat_step = None
        self._lon_step = None
        self._lat_ticks = None
        self._lon_ticks = None
        if zoom is None:
            self._zoom = int(8.75-np.log2(np.diff(lat_lim)))
        else:
            self._zoom = zoom
        self.im = []
        for url in smurl:
            a, bbox = getImageCluster(
                *lat_lim, *lon_lim, zoom, smurl=url, out_file=out_file
            )
            self.im.append(ax.imshow(
                a, extent=bbox, interpolation='lanczos', origin='upper'))

        lon_t, lon_tl = get_dms_ticks(
            lon_lim, lon_step, dg_fmt=dg_fmt, mn_fmt=xmn_fmt,
            sc_fmt=sc_fmt, precision=precision
        )
        lat_t, lat_tl = get_dms_ticks(
            lat_lim, lat_step, dg_fmt=dg_fmt, mn_fmt=ymn_fmt,
            sc_fmt=sc_fmt, precision=precision
        )

        if do_label:
            ax.set(
                xticks=lon2num(lon_t),
                xticklabels=lon_tl,
                yticks=lat2num(lat_t),
                yticklabels=lat_tl,
                # xlabel='longitude',
                # ylabel='latitude',
            )

        ax.set(
            aspect=1,
            # adjustable='datalim',
            xlim=lon2num(lon_lim),
            ylim=lat2num(lat_lim),
        )

        if dist is not None:
            self.dist(dist)

    def plot(self, lat, lon, *args, **kwargs):
        return self.ax.plot(lon2num(np.array(lon)), lat2num(np.array(lat)),
                            *args, **kwargs)

    def scatter(self, lat, lon, *args, **kwargs):
        return self.ax.scatter(lon2num(np.array(lon)), lat2num(np.array(lat)),
                               *args, **kwargs)

    def text(self, lat, lon, *args, **kwargs):
        return self.ax.text(lon2num(lon), lat2num(lat),
                            *args, **kwargs)

    def contour(self, lat, lon, *args, **kwargs):
        return self.ax.contour(lon2num(lon), lat2num(lat),
                               *args, **kwargs)

    def contourf(self, lat, lon, *args, **kwargs):
        return self.ax.contourf(lon2num(lon), lat2num(lat),
                                *args, **kwargs)

    def fill(self, lat, lon, *args, **kwargs):
        return self.ax.fill(lon2num(np.array(lon)), lat2num(np.array(lat)),
                            *args, **kwargs)

    def imshow(self, *args, **kwargs):
        if 'extent' in kwargs:
            kwargs['extent'] = (
                lon2num(kwargs['extent'][0]),
                lon2num(kwargs['extent'][1]),
                lat2num(kwargs['extent'][2]),
                lat2num(kwargs['extent'][3]),
            )
        return self.ax.imshow(*args, **kwargs)

    def rect(self, lat, lon, *args, **kwargs):
        return self.plot(
            np.r_[lat[0], lat[1], lat[1], lat[0], lat[0]],
            np.r_[lon[0], lon[0], lon[1], lon[1], lon[0]],
            *args, **kwargs,
        )

    def annotate(self, text_, latlon, *args, **kwargs):
        return self.ax.annotate(
            text_, xy=(lon2num(latlon[1]), lat2num(latlon[0])),
            *args, **kwargs)

    def dist(self, km=None, **kwargs):
        if km is None:
            km = 12800 * 2**self._zoom
        dist_lbl = f'{km} km'
        km *= 360 / 40075 / np.cos(self._lat_lim[0] * np.pi / 180)
        d = .05
        self.plot(
            self._lat_lim @ np.r_[1 - d, d] + km * np.r_[0, 0],
            self._lon_lim @ np.r_[1 - d, d] + km * np.r_[0, 1],
            'k-|', lw=3, solid_capstyle='butt', clip_on=False,
            **kwargs,
        )
        self.annotate(dist_lbl, latlon=(
            self._lat_lim @ np.r_[1 - d, d],
            self._lon_lim @ np.r_[1 - d, d] + km * .5,
        ), ha='center', va='center', xytext=(0, 10),
            textcoords='offset points',
            bbox=dict(facecolor=[1, 1, 1, .5],
                      edgecolor='none', boxstyle='round'),
            **kwargs,
        )

    @property
    def lat_lim(self):
        """I'm the 'x' property."""
        print("getter of x called")
        return self._x

    @lat_lim.setter
    def lat_lim(self, value):
        print("setter of x called")
        self._x = value


if __name__ == '__main__':
    import sys
    fig, ax = plt.subplots(1, 1)

    m = map(
        lat_lim=(-60, 80), lon_lim=(-220, 120), lat_step=10, lon_step=30,
        zoom=3, xmn_fmt='', ymn_fmt='',
        smurl=[
            "http://tile.openstreetmap.org/{0}/{1}/{2}.png",
            "https://tiles.openseamap.org/seamark/{0}/{1}/{2}.png",
        ], ax=ax,
    )

    m.plot((-60, 0, 60), (-60, 60, -60))
    m.fill((-60, 0, 60), (-60, 60, -60))

    m.dist(2_000)
    fig.savefig(sys.argv[0][:-2] + 'png')
    # plt.show()
